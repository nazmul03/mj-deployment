import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function LinearProgressWithLabel(props) {
  return (
    <Box display='flex' alignItems='center'>
      <Box width='100%' mr={1}>
        <LinearProgress variant='determinate' {...props} />
      </Box>
      <Box minWidth={35}>
        <Typography variant='body2' color='textSecondary'>{`${Math.round(props.value)}%`}</Typography>
      </Box>
    </Box>
  );
}

LinearProgressWithLabel.propTypes = {
  /**
   * The value of the progress indicator for the determinate and buffer variants.
   * Value between 0 and 100.
   */
  value: PropTypes.number.isRequired
};

const useStyles = makeStyles({
  root: {
    width: '100%'
  },
  bar: {
    animationDuration: '1000ms',
    borderRadius: 10
  }
});

export default function Progress(props) {
  const classes = useStyles();
  const [progress, setProgress] = React.useState(10);
  const [buffer, setBuffer] = React.useState(10);

  const progressRef = React.useRef(() => {});
  React.useEffect(() => {
    progressRef.current = () => {
      if (progress > 100) {
        setProgress(0);
        setBuffer(10);
      } else {
        const diff = Math.random() * 10;
        const diff2 = Math.random() * 10;
        setProgress(progress + diff);
        setBuffer(progress + diff + diff2);
      }
    };
  });

  React.useEffect(() => {
    const timer = setInterval(() => {
      // for progress with label
      // setProgress((prevProgress) => (prevProgress >= 100 ? 10 : prevProgress + 10));

      // for buffer progress
      progressRef.current();
    }, 800);
    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <div className={classes.root}>
      {props.variant === 'linearWithLabel' ? (
        <LinearProgressWithLabel className={classes.bar} value={progress} />
      ) : props.variant === 'linearWithBuffer' ? (
        <LinearProgress className={classes.bar} variant='buffer' value={progress} valueBuffer={buffer} />
      ) : props.variant === 'circular' ? (
        <CircularProgress className={classes.bar} thickness={props.thickness} size={props.size} color={props.color} />
      ) : (
        <LinearProgress className={classes.bar} thickness={props.thickness} size={props.size} color={props.color} />
      )}
    </div>
  );
}
