import React from 'react';

const FileUploader = (props) => {
  const onFileSelect = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      if (props.openMediaOptions) {
        props.setOpenMediaOptions(false);
      }
      props.selectFile(e.target.files);
    }
  };

  return (
    <div>
      <label style={{ width: '100%' }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            fontSize: '1.25rem'
          }}
        >
          {props.children}
        </div>
        <input
          type='file'
          name='file'
          id='file'
          multiple
          accept='image/*,audio/*,video/*'
          onChange={onFileSelect}
          style={{ display: 'none' }}
        />
      </label>
    </div>
  );
};

export default FileUploader;
