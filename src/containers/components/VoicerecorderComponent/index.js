import React from 'react';
import axios from 'axios';

import CustomModal from '../CustomModal';
import Recorder from './Recorder';
import { generateNameFromUrl } from '../../utils';

const VoicerecorderComponent = (props) => {
  const [state, setState] = React.useState({
    audioDetails: {
      url: null,
      blob: null,
      chunks: null,
      duration: {
        h: 0,
        m: 0,
        s: 0
      }
    }
  });

  const [recordComplete, setRecordComplete] = React.useState(false);

  const handleAudioUpload = (blob, url) => {
    props.setOpenRecorder(false);
    props.setOpenMediaOptions(false);
    blob.name = generateNameFromUrl('RecorderAudio', 'mp3');
    blob.url = url;

    props.setUploading([...props.uploading, blob.name]);

    console.log(blob);

    let formData = new FormData();
    formData.append('file', blob, blob.name);

    props.setUploadProcessing(true);

    axios
      .post('https://api.miljulapp.com/api/v1//common/fileUpload', formData)
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          blob.media_url = response.data.url;
          props.setUploading([]);
          props.setUploadProcessing(false);
        } else {
          props.setUploading([]);
          props.setMessage('Error in File Upload Process');
          props.setUploadProcessing(false);
        }
      })
      .catch((error) => {
        console.log(error);
        props.setUploading([]);
        props.setMessage('Error in File Upload Process');
        props.setUploadProcessing(false);
      });

    props.setFileObj((initialFiles) => {
      return [...initialFiles, blob];
    });

    window.URL.revokeObjectURL(url);
  };

  const handleAudioStop = (data) => {
    setState({ audioDetails: data });
    setRecordComplete(true);
  };

  const handleRest = () => {
    const reset = {
      url: null,
      blob: null,
      chunks: null,
      duration: {
        h: 0,
        m: 0,
        s: 0
      }
    };
    setState({ audioDetails: reset });
  };

  return (
    <>
      <div>{props.children}</div>
      <CustomModal
        open={props.openRecorder}
        setOpen={props.setOpenRecorder}
        theme={props.theme}
        closeButtonTop='-7%'
        closeButtonLeft='91%'
      >
        <Recorder
          record={true}
          audioURL={state.audioDetails.url}
          showUIAudio
          recordComplete={recordComplete}
          handleAudioStop={(data) => handleAudioStop(data)}
          handleAudioUpload={handleAudioUpload}
          handleRest={() => handleRest()}
          theme={props.theme}
        />
      </CustomModal>
    </>
  );
};

export default VoicerecorderComponent;
