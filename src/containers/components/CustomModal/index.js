import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import CloseIcon from '@material-ui/icons/Close';
import { isMobile } from 'react-device-detect';

import Box from '../Box';

const useStyles = makeStyles(() => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: (props) => props.backgroundColor || 'transparent',
    padding: (props) => props.padding || '1rem 2rem',
    position: 'relative',
    borderRadius: (props) => props.borderRadius || '5px',
    width: (props) => props.width || (isMobile ? '80vw' : 'fit-content'),
    height: (props) => props.height || 'fit-content',
    '&:hover, &:focus': {
      outline: 'none'
    }
  },
  modalHeader: {
    position: 'absolute',
    top: (props) => props.closeButtonTop || '-12%',
    left: (props) => props.closeButtonLeft || '93%',
    width: 'fit-content',
    margin: '5px 0 5px auto',
    '&:hover, &:focus': {
      outline: 'none'
    }
  }
}));

export default function CustomModal(props) {
  const classes = useStyles({
    backgroundColor: props.backgroundColor || props.theme.contentBackgroundColor,
    padding: props.padding,
    borderRadius: props.borderRadius,
    closeButtonTop: props.closeButtonTop,
    closeButtonLeft: props.closeButtonLeft,
    width: props.width,
    height: props.height
  });

  return (
    <div>
      <Modal
        aria-labelledby='transition-modal-title'
        aria-describedby='transition-modal-description'
        className={classes.modal}
        open={props.open}
        onClose={() => props.setOpen(false)}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={props.open}>
          <div className={classes.paper}>
            <div className={classes.modalHeader}>
              <Box
                says={<CloseIcon />}
                padding='4px'
                borderRadius='50%'
                does={() => props.setOpen(false)}
                boxShadow='0'
                theme={props.theme}
                hoverBoxShadow='0px 0px 3px 1px #ddd'
              />
            </div>
            {props.children}
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
