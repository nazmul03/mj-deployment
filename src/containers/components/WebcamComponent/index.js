import React from 'react';
import axios from 'axios';
import Webcam from 'react-webcam';
import { makeStyles } from '@material-ui/core/styles';
import { isMobile } from 'react-device-detect';

import CustomModal from '../CustomModal';
import Box from '../Box';
import { generateNameFromUrl } from '../../utils';

const useStyles = makeStyles((theme) => ({
  webcam: {
    backgroundColor: 'transparent',
    width: isMobile ? '90vw' : '50vmax',
    height: isMobile ? '40vh' : '40vmax',
    borderRadius: '10px',
    '&:hover, &:focus': {
      outline: 'none'
    }
  }
}));

const WebcamComponent = (props) => {
  const classes = useStyles();

  const webcamRef = React.useRef(null);
  const mediaRecorderRef = React.useRef(null);
  const [capturing, setCapturing] = React.useState(false);
  const [recordedChunks, setRecordedChunks] = React.useState([]);
  const [paused, setPaused] = React.useState(false);
  const [time, setTime] = React.useState({
    h: 0,
    m: 0,
    s: 0
  });
  let timer = React.useRef(0);

  const countDown = () => {
    setTime((prev) => {
      let secs = prev.s + 1;
      let hours = Math.floor(secs / (60 * 60));

      let divisor_for_minutes = secs % (60 * 60);
      let minutes = Math.floor(divisor_for_minutes / 60);

      let divisor_for_seconds = divisor_for_minutes % 60;
      let seconds = Math.ceil(divisor_for_seconds);
      return {
        h: hours,
        m: minutes,
        s: seconds
      };
    });
  };

  const startTimer = () => {
    timer.current = setInterval(countDown, 1000);
  };

  const handleDataAvailable = ({ data }) => {
    if (data.size > 0) {
      setRecordedChunks((prev) => prev.concat(data));
    }
  };

  const handleStartCaptureClick = () => {
    setCapturing(true);
    startTimer();
    let options = {
      mimeType: 'video/webm'
    };
    mediaRecorderRef.current = new MediaRecorder(webcamRef.current.stream, options);
    mediaRecorderRef.current.addEventListener('dataavailable', handleDataAvailable);
    mediaRecorderRef.current.start(10);
  };

  const handlePauseCaptureClick = () => {
    clearInterval(timer.current);
    mediaRecorderRef.current.pause();
    setPaused(true);
  };

  const handleResumeCaptureClick = () => {
    setPaused(false);
    startTimer();
    mediaRecorderRef.current.resume();
  };

  const handleStopCaptureClick = () => {
    clearInterval(timer.current);
    mediaRecorderRef.current.stop();
    setCapturing(false);
    setTime({
      h: 0,
      m: 0,
      s: 0
    });

    handleUpload();
  };

  const handleUpload = () => {
    if (recordedChunks.length) {
      props.setOpenCam(false);
      props.setOpenMediaOptions(false);
      const blob = new Blob(recordedChunks, {
        type: 'video/mp4'
      });
      console.log(blob);
      const url = URL.createObjectURL(blob);
      blob.name = generateNameFromUrl('WebcamVideo', 'mp4');
      blob.url = url;

      props.setUploading([...props.uploading, blob.name]);

      console.log(blob);

      let formData = new FormData();
      formData.append('file', blob, blob.name);

      props.setUploadProcessing(true);

      axios
        .post('https://api.miljulapp.com/api/v1/common/fileUpload', formData)
        .then((response) => {
          console.log(response);
          if (response.status === 200) {
            blob.media_url = response.data.url;
            props.setUploading([]);
            props.setUploadProcessing(false);
          } else {
            props.setUploading([]);
            props.setMessage('Error in File Upload Process');
            props.setUploadProcessing(false);
          }
        })
        .catch((error) => {
          console.log(error);
          props.setUploading([]);
          props.setMessage('Error in File Upload Process');
          props.setUploadProcessing(false);
        });

      props.setFileObj((initialFiles) => {
        return [...initialFiles, blob];
      });

      window.URL.revokeObjectURL(url);
      setRecordedChunks([]);
    }
  };

  return (
    <div>
      {props.children}
      <div>
        <CustomModal
          open={props.openCam}
          setOpen={props.setOpenCam}
          theme={props.theme}
          closeButtonTop='-5%'
          closeButtonLeft='95%'
          width='fit-content'
          padding='5px'
        >
          <>
            <div>
              <Webcam audio={true} ref={webcamRef} className={classes.webcam} mirrored />
            </div>
            <div className='row'>
              <div className='col-auto'>
                <div>
                  {capturing ? (
                    <div>
                      <Box says='Stop' margin='0 2rem 0 0' does={handleStopCaptureClick} theme={props.theme} />
                      {paused ? (
                        <Box says='Resume' does={handleResumeCaptureClick} theme={props.theme} />
                      ) : (
                        <Box says='Pause' does={handlePauseCaptureClick} theme={props.theme} />
                      )}
                    </div>
                  ) : (
                    <div>
                      <Box says='Record' margin='0 2rem 0 0' does={handleStartCaptureClick} theme={props.theme} />
                      {/* {recordedChunks.length > 0 && <Box says='Done' does={handleUpload} theme={props.theme} />} */}
                    </div>
                  )}
                </div>
              </div>
              <div className='col-2'>
                <div
                  style={{
                    fontSize: '2rem',
                    textAlign: 'center',
                    color: props.theme.headerTextColor
                  }}
                >
                  <span>{time.m !== undefined ? `${time.m <= 9 ? '0' + time.m : time.m}` : '00'}</span>
                  <span>:</span>
                  <span>{time.s !== undefined ? `${time.s <= 9 ? '0' + time.s : time.s}` : '00'}</span>
                </div>
              </div>
            </div>
          </>
        </CustomModal>
      </div>
    </div>
  );
};

export default WebcamComponent;
