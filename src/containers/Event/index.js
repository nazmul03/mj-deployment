import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';

import Content from '../Content';
import CustomModal from '../components/CustomModal';
import Box from '../components/Box';
import { isFbOrInstaAndroidBrowser } from '../utils';

const Event = (props) => {
  const [data, setData] = React.useState(null);
  const [message, setMessage] = React.useState(null);

  // eslint-disable-next-line
  const [colabEventId, setColabEventId] = React.useState(props && props.match.params.eventId);

  const REDIRECT_URL = `https://mj-test-deploy.netlify.app/#/event/${colabEventId}`;

  React.useEffect(() => {
    console.log(REDIRECT_URL);
    if (isFbOrInstaAndroidBrowser()) {
      window.location.assign(`https://api.miljulapp.com/api/v1/common/ifAppRedirect?url=${REDIRECT_URL}`);
    } else {
      colabEventId &&
        axios
          .get(`https://api.miljulapp.com/api/v1/api/v1/collab/collabevent/${colabEventId}`)
          .then((response) => {
            console.log(response);
            if (response.status === 200) {
              setData(response.data.response.response);
            } else {
              setMessage('There was some error');
            }
          })
          .catch((error) => {
            console.log(error);
            setMessage('There was some error');
          });
    }
  }, [colabEventId, REDIRECT_URL]);

  return (
    <>
      {/* {navigator.platform}
      {navigator.userAgent} */}
      <Content
        data={data && data}
        message={message}
        setMessage={setMessage}
        colabEventId={colabEventId && colabEventId}
        theme={props.theme}
      />
      <CustomModal open={message} setOpen={() => setMessage(null)} theme={props.theme}>
        <div style={{ textAlign: 'center' }}>
          <div style={{ fontSize: '2rem', padding: '0.5rem 0 2rem' }}>{message}</div>
          <Box says='Ok' does={() => setMessage(null)} theme={props.theme} />
        </div>
      </CustomModal>
    </>
  );
};

export default withRouter(Event);
