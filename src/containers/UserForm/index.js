import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { isMobile } from 'react-device-detect';

import Box from '../components/Box';

const useStyles = makeStyles(() => ({
  singleInputArea: {
    padding: '0.5rem 0',
    height: '90px',
    position: 'relative'
  },
  label: {
    fontSize: '1.1rem',
    color: (theme) => theme.bodyTextColor,
    fontWeight: '400'
  },
  inputField: {
    backgroundColor: '#F6F7F9',
    border: 'none',
    padding: '0.5rem',
    borderRadius: '5px',
    fontSize: '0.8rem',
    width: '100%',
    '&:hover, &:focus': {
      outline: 'none'
    },
    '&::placeholder': {
      color: '#C3C8CC'
    }
  },
  terms: {
    transform: 'scale(1.5)'
    // '& input': {
    //   display: 'none'
    // },
    // '&$checked': {
    //   backgroundColor: '#B82318',
    //   background: '#B82318',
    //   color: '#B82318',
    //   '& checkmark': {
    //     backgroundColor: '#B82318',
    //     background: '#B82318',
    //     color: '#B82318'
    //   }
    // }
  }
}));

const UserForm = (props) => {
  const classes = useStyles(props && props.theme);
  const [terms, setTerms] = React.useState(true);
  // const initialUser = {
  //   name: '',
  //   email: '',
  //   mobile: ''
  // };

  const handleChange = (e) => {
    props.setUser({
      ...props.user,
      [e.target.id]: e.target.value
    });
  };

  const handleSocialMediaChange = (e) => {
    props.setUser({
      ...props.user,
      meta_data: {
        [e.target.id]: e.target.value
      }
    });
  };

  const handleTerms = (e) => {
    setTerms(!terms);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (props.user.name === '' || props.user.email === '') {
      props.setMessage('Please fill all the fields');
    } else if (terms === false) {
      props.setMessage('Please agree with terms and conditions');
    } else {
      props.createCollaborator();
      // props.setUser(initialUser);
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className='row'>
          <div className='col-12 col-sm-6'>
            <div className={classes.singleInputArea}>
              <div>
                <label htmlFor='name' className={classes.label}>
                  Your Name
                </label>
              </div>
              <div>
                <input
                  type='text'
                  name='name'
                  id='name'
                  value={props.user.name}
                  className={classes.inputField}
                  placeholder='Delip Kumar'
                  onChange={handleChange}
                />
              </div>
            </div>
          </div>
          <div className='col-12 col-sm-6'>
            <div className={classes.singleInputArea}>
              <div>
                <label htmlFor='email' className={classes.label}>
                  Your Email
                </label>
              </div>
              <div>
                <input
                  type='email'
                  name='email'
                  id='email'
                  value={props.user.email}
                  className={classes.inputField}
                  placeholder='delipkumar@gmail.com'
                  onChange={handleChange}
                />
              </div>
            </div>
          </div>
        </div>
        <div className='row'>
          <div className='col-12 col-sm-6'>
            <div className={classes.singleInputArea}>
              <div>
                <label htmlFor='mobile' className={classes.label}>
                  Your Social Media
                </label>
              </div>
              <div>
                <input
                  type='text'
                  name='social_media'
                  id='social_media'
                  value={props.user.meta_data.social_media}
                  className={classes.inputField}
                  placeholder='DelipKumar (Optional)'
                  onChange={handleSocialMediaChange}
                />
              </div>
            </div>
          </div>
          <div className='col-12 col-sm-6'>
            <div className={classes.singleInputArea}>
              <div style={{ marginTop: isMobile ? 'auto' : '40px', marginLeft: '10px' }}>
                <input
                  type='checkbox'
                  name='terms'
                  id='terms'
                  value={terms}
                  checked={terms}
                  onChange={handleTerms}
                  className={classes.terms}
                />
                <label
                  htmlFor='checkbox'
                  style={{
                    color: props.theme.bodyTextColor,
                    fontSize: '0.75rem',
                    height: 'fit-content',
                    marginLeft: '10px'
                  }}
                >
                  <a
                    style={{ color: 'inherit', textDecoration: 'underline' }}
                    href='https://miljulapp.com/legal/terms-of-service.html'
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    Agree with Terms & Conditions
                  </a>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div className='row'>
          <div style={{ margin: '1rem auto 0 ', width: 'fit-content' }}>
            <Box says='Submit' type='submit' theme={props.theme} borderRadius='5px' padding='8px 64px' />
          </div>
        </div>
      </form>
    </>
  );
};

export default UserForm;
