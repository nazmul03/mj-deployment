import React from 'react';

import BottomLogo from '../../assets/images/bottom-logo.png';

const Footer = (props) => {
  return (
    <div
      style={{
        backgroundColor: props.theme.backgroundColor,
        textAlign: 'center'
      }}
    >
      <div style={{ padding: '2rem 0', display: 'inline-block' }}>
        <span
          style={{
            color: props.theme.bodyTextColor,
            fontSize: '0.75rem',
            fontWeight: '600',
            height: 'fit-content',
            margin: 'auto 1rem'
          }}
        >
          Powered by
        </span>
        <a href='https://forms.gle/TsvDnqHjeQk46g7Q7'>
          <img src={BottomLogo} alt='Logo' />
        </a>
      </div>
    </div>
  );
};

export default Footer;
